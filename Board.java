public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString(){
		String tiles = "";
		for(int i = 0; i < closedTiles.length; i++){
			if (closedTiles[i]){
				tiles = tiles + "X ";
			}
			else{
				tiles = tiles + (i+1) + " ";
			}
		}
		return tiles;
	}
	
	public boolean playATurn(){
		
		boolean gameOver;
		int sum = 0;
		
		this.die1.roll();
		this.die2.roll();
		
		System.out.println(this.die1.toString());
		System.out.println(this.die2.toString());
		
		sum = this.die1.getPips() + this.die2.getPips();
		
		if( !closedTiles[sum-1] ){
			System.out.println("Closing tile: " + sum);
			System.out.println("");
			closedTiles[sum-1] = true;
			gameOver = false;
		}
		else {
			System.out.println("Tile " + sum + " is already shut.");
			System.out.println("");
			gameOver = true;
		}
		return gameOver;
		
	}
	
}