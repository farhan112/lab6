public class ShutTheBox{
	
	public static void main(String[] args){
		
		System.out.println("Welcome to a fun game of Shut the Box!");
		
		Board board = new Board();
		boolean gameOver = false;
		
		while (!gameOver){
			System.out.println("Player 1's turn");
			System.out.println(board.toString());
			
			gameOver = board.playATurn();
			if (gameOver){
				System.out.println("Player 2 wins!");
			}
			else{
				System.out.println("Player 2's turn");
				System.out.println(board.toString());
				
				gameOver = board.playATurn();
				if (gameOver){
					System.out.println("Player 1 wins!");
				}
			}
		}
	}
	
}